package com.incode.tiger.country.service;

import java.util.List;

import com.incode.tiger.country.domain.Country;
import com.incode.tiger.country.domain.CountryQuery;
import com.incode.tiger.country.repo.CountryRepository;
import com.incode.tiger.exception.LocaleNotSupportedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CountryServiceImpl implements CountryService
{
    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository)
    {
        this.countryRepository = countryRepository;
    }

    @Override
    @Cacheable(cacheNames = "country")
    public List<Country> search(CountryQuery query)
    {

        CountryQuery existsQuery = new CountryQuery();
        existsQuery.setLocale(query.getLocale());
        if(!countryRepository.exists(existsQuery))
        {
            throw new LocaleNotSupportedException(query.getLocale());
        }
        return countryRepository.search(query);
    }

}
