package com.incode.tiger.country.service;

import java.util.List;

import com.incode.tiger.country.domain.Country;
import com.incode.tiger.country.domain.CountryQuery;

public interface CountryService
{
    List<Country> search(CountryQuery query);
}
