package com.incode.tiger.country.repo;

import java.util.List;
import java.util.stream.Collectors;

import com.incode.tiger.country.domain.Country;
import com.incode.tiger.country.domain.CountryQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class CountryRepositoryImpl implements CountryRepository
{
    private MongoTemplate mongoTemplate;

    @Autowired
    public CountryRepositoryImpl(MongoTemplate mongoTemplate)
    {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<Country> search(CountryQuery query)
    {
        Query mongoQuery = buildQuery(query);
        return mongoTemplate.find(mongoQuery, Country.class);
    }

    @Override
    public boolean exists(CountryQuery query)
    {
        Query mongoQuery = buildQuery(query);
        return mongoTemplate.exists(mongoQuery, Country.class);
    }


    private Query buildQuery(CountryQuery countryQuery)
    {
        Query query = new Query();
        Criteria localeCriteria = Criteria.where(Country.LOCALE_FIELD).is(countryQuery.getLocale().toLowerCase());
        query.addCriteria(localeCriteria);
        List<String> countryCodes = countryQuery.getCountryCodes()
                .stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        if(!countryCodes.isEmpty())
        {
            Criteria countryCodesCriteria = new Criteria();
            countryCodesCriteria.orOperator(Criteria.where(Country.ALPHA_2_CODE).in(countryCodes),
                    Criteria.where(Country.ALPHA_3_CODE).in(countryCodes));
            query.addCriteria(countryCodesCriteria);
        }
        return query;
    }
}
