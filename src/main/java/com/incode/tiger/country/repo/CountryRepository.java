package com.incode.tiger.country.repo;

import java.util.List;

import com.incode.tiger.country.domain.Country;
import com.incode.tiger.country.domain.CountryQuery;

public interface CountryRepository
{
    List<Country> search(CountryQuery query);

    boolean exists(CountryQuery query);
}
