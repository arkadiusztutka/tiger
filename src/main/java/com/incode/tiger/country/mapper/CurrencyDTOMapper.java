package com.incode.tiger.country.mapper;

import com.incode.tiger.country.domain.Currency;
import com.incode.tiger.country.dto.CurrencyDTO;
import com.incode.tiger.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class CurrencyDTOMapper implements Mapper<Currency, CurrencyDTO>
{
    @Override
    public CurrencyDTO map(Currency source)
    {
        CurrencyDTO target = new CurrencyDTO();
        target.setName(source.getName());
        target.setIso4217Code(source.getIso4217Code());
        return target;
    }
}
