package com.incode.tiger.country.mapper;

import com.incode.tiger.country.domain.Language;
import com.incode.tiger.country.dto.LanguageDTO;
import com.incode.tiger.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class LanguageDTOMapper implements Mapper<Language, LanguageDTO>
{
    @Override
    public LanguageDTO map(Language source)
    {
        LanguageDTO target = new LanguageDTO();
        target.setName(source.getName());
        target.setIso6391Code(source.getIso6391Code());
        return target;
    }
}
