package com.incode.tiger.country.mapper;

import com.incode.tiger.country.domain.CountryQuery;
import com.incode.tiger.country.dto.CountryQueryDTO;
import com.incode.tiger.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class CountryQueryMapper implements Mapper<CountryQueryDTO, CountryQuery>
{
    @Override
    public CountryQuery map(CountryQueryDTO source)
    {
        CountryQuery target = new CountryQuery();
        target.setLocale(source.getLocale());
        target.setCountryCodes(source.getCountryCodes());
        return target;
    }
}
