package com.incode.tiger.country.mapper;

import java.util.List;

import com.incode.tiger.country.domain.Country;
import com.incode.tiger.country.domain.Currency;
import com.incode.tiger.country.domain.Language;
import com.incode.tiger.country.dto.CountryDTO;
import com.incode.tiger.country.dto.CurrencyDTO;
import com.incode.tiger.country.dto.LanguageDTO;
import com.incode.tiger.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CountryDTOMapper implements Mapper<Country, CountryDTO>
{
    private Mapper<Language, LanguageDTO> languageDTOMapper;
    private Mapper<Currency, CurrencyDTO> currencyDTOMapper;

    @Autowired
    public CountryDTOMapper(Mapper<Language, LanguageDTO> languageDTOMapper, Mapper<Currency, CurrencyDTO> currencyDTOMapper)
    {
        this.languageDTOMapper = languageDTOMapper;
        this.currencyDTOMapper = currencyDTOMapper;
    }

    @Override
    public CountryDTO map(Country source)
    {
        CountryDTO target = new CountryDTO();
        target.setName(source.getName());
        target.setAlpha2Code(source.getAlpha2Code());
        target.setAlpha3Code(source.getAlpha3Code());
        List<LanguageDTO> languages = languageDTOMapper.map(source.getLanguages());
        target.setLanguages(languages);
        List<CurrencyDTO> currencies = currencyDTOMapper.map(source.getCurrencies());
        target.setCurrencies(currencies);
        target.setFlag(source.getFlag());
        return target;
    }
}
