package com.incode.tiger.country.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "country")
@Setter
@Getter
public class Country implements Serializable
{

    public static final String ALPHA_2_CODE = "alpha2code";
    public static final String ALPHA_3_CODE = "alpha3code";
    public static final String NAME_FIELD = "name";
    public static final String LOCALE_FIELD = "locale";
    public static final String FLAG_FIELD = "flag";

    @Id
    private ObjectId id;

    @Field(NAME_FIELD)
    private String name;

    @Field(ALPHA_2_CODE)
    private String alpha2Code;//Country code

    @Field(ALPHA_3_CODE)
    private String alpha3Code;//Country code

    private List<Language> languages = new ArrayList<>();

    private List<Currency> currencies = new ArrayList<>();

    @Field(LOCALE_FIELD)
    @Indexed
    private String locale;

    @Field(FLAG_FIELD)
    private String flag;
}
