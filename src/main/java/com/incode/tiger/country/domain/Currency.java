package com.incode.tiger.country.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
public class Currency implements Serializable
{
    public static final String ISO_4217_CODE_FIELD = "iso4217code";
    public static final String NAME_FIELD = "name";

    @Field(ISO_4217_CODE_FIELD)
    private String iso4217Code;

    @Field(NAME_FIELD)
    private String name;
}
