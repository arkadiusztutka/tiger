package com.incode.tiger.country.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class CountryQuery
{
    private String locale;

    private List<String> countryCodes = new ArrayList<>();
}
