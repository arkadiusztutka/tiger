package com.incode.tiger.country.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
public class Language implements Serializable
{
    public static final String ISO_639_1_CODE_FIELD = "iso6391code";
    public static final String NAME_FIELD = "name";

    @Field(ISO_639_1_CODE_FIELD)
    private String iso6391Code;

    @Field(NAME_FIELD)
    private String name;
}
