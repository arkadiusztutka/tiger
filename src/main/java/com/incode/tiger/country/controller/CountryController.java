package com.incode.tiger.country.controller;

import java.util.List;

import com.incode.tiger.country.domain.Country;
import com.incode.tiger.country.domain.CountryQuery;
import com.incode.tiger.country.dto.CountryDTO;
import com.incode.tiger.country.dto.CountryQueryDTO;
import com.incode.tiger.country.service.CountryService;
import com.incode.tiger.mapper.Mapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "Retrieving information about countries")
@RestController
public class CountryController
{
    private CountryService countryService;
    private Mapper<Country, CountryDTO> countryDTOMapper;
    private Mapper<CountryQueryDTO, CountryQuery> countryQueryMapper;

    @Autowired
    public CountryController(CountryService countryService, Mapper<Country, CountryDTO> countryDTOMapper, Mapper<CountryQueryDTO, CountryQuery> countryQueryMapper)
    {
        this.countryService = countryService;
        this.countryDTOMapper = countryDTOMapper;
        this.countryQueryMapper = countryQueryMapper;
    }

    @ApiOperation(value = "Searches countries", response = CountryDTO.class, responseContainer = "List")
    @ApiResponse(code = 400, message = "Bad request")
    @GetMapping(path = "/countries")
    public List<CountryDTO> searchCountries(CountryQueryDTO queryDTO)
    {
        CountryQuery query = countryQueryMapper.map(queryDTO);
        List<Country> countries = countryService.search(query);
        return countryDTOMapper.map(countries);
    }
}
