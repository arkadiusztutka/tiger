package com.incode.tiger.country.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "Country", description = "Country information")
@Setter
@Getter
public class CountryDTO implements Serializable
{
    @ApiModelProperty(value = "Name")
    private String name;

    @ApiModelProperty(value = "Alpha-2 code")
    private String alpha2Code;

    @ApiModelProperty(value = "Alpha-3 code")
    private String alpha3Code;

    @ApiModelProperty(value = "Languages used in country")
    private List<LanguageDTO> languages = new ArrayList<>();

    @ApiModelProperty(value = "Currencies used in country")
    private List<CurrencyDTO> currencies = new ArrayList<>();

    @ApiModelProperty(value = "Link to flag")
    private String flag;
}
