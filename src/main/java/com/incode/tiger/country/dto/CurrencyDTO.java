package com.incode.tiger.country.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "Currency", description = "Currency information")
@Getter
@Setter
public class CurrencyDTO implements Serializable
{
    @ApiModelProperty(value = "ISO 4217 code")
    private String iso4217Code;

    @ApiModelProperty(value = "Name")
    private String name;
}
