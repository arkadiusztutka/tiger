package com.incode.tiger.country.dto;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "CountryQuery", description = "Country query")
@Getter
@Setter
public class CountryQueryDTO
{
    @ApiModelProperty(value = "Locale")
    @NotBlank(message = "validation.missing.locale")
    private String locale;

    @ApiModelProperty(value = "Country codes(in Alpha-2 or Alpha-3) for countries wanted to be returned")
    private List<String> countryCodes = new ArrayList<>();
}
