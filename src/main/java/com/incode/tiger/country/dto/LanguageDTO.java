package com.incode.tiger.country.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "Language", description = "Language information")
@Getter
@Setter
public class LanguageDTO implements Serializable
{

    @ApiModelProperty(value = "ISO 639-1 code")
    private String iso6391Code;

    @ApiModelProperty(value = "Name")
    private String name;
}
