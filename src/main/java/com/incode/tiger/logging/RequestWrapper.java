package com.incode.tiger.logging;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RequestWrapper extends HttpServletRequestWrapper
{
   private final String body;

   public RequestWrapper(HttpServletRequest request) throws IOException
   {
      super(request);
      StringBuilder stringBuilder = new StringBuilder();
      InputStream inputStream = request.getInputStream();
      if (inputStream != null)
      {
         try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)))
         {
            char[] charBuffer = new char[128];
            int bytesRead;
            while ((bytesRead = bufferedReader.read(charBuffer)) > 0)
            {
               stringBuilder.append(charBuffer, 0, bytesRead);
            }
         }
      } else
      {
         stringBuilder.append("");
      }

      body = stringBuilder.toString();
   }

   String getBody()
   {
      return body;
   }
}
