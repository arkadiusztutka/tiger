package com.incode.tiger.exception;

import lombok.Getter;

@Getter
public class LocaleNotSupportedException extends RuntimeException
{
    private String locale;

    public LocaleNotSupportedException(String locale)
    {
        super();
        this.locale = locale;
    }
}
