package com.incode.tiger.exception;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler
{
    private final MessageSource messageSource;

    @Autowired
    public GlobalControllerExceptionHandler(MessageSource messageSource)
    {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(value = LocaleNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleLocaleNotSupportedException(HttpServletRequest req, LocaleNotSupportedException ex)
    {
        log.error("Locale error", ex);
        return ErrorResponse.create(HttpStatus.BAD_REQUEST, getMessage("error.localenotsupported.msg", ex.getLocale()));
    }

    @ExceptionHandler(value = BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleValidationException(HttpServletRequest req, BindException ex)
    {
        log.error("Validation error", ex);
        String defaultMessage = ex.getFieldError().getDefaultMessage();
        return ErrorResponse.create(HttpStatus.BAD_REQUEST, defaultMessage);
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleUnknownException(Exception ex)
    {
        log.error("Internal application error", ex);
        return ErrorResponse.create(HttpStatus.INTERNAL_SERVER_ERROR, getMessage("error.unknown.msg"));
    }

    private String getMessage(String key, Object... objects)
    {
        return messageSource.getMessage(key, objects, Locale.ENGLISH);
    }
}
