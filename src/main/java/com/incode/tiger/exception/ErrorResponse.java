package com.incode.tiger.exception;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class ErrorResponse
{
   private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

   private HttpStatus status;
   private String errorMessage;
   private String dateTime;

   private ErrorResponse(HttpStatus status, String errorMessage)
   {
      this.errorMessage = errorMessage;
      this.status = status;
      this.dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
   }

   public static ErrorResponse create(HttpStatus status, String errorMessage)
   {
      return new ErrorResponse(status, errorMessage);
   }
}
